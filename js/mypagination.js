/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//mypagination Class/Plugin

(function($){

 	$.fn.extend({ 
 		
		//pass the options variable to the function
 		myPagination: function(options) {
                        
                        var obj;
                        var id;
                        var context;
                        var page;
                        var length;
                        var cols;
                        var alldata=[];
                        var sortBy;
                        var pageCount=5;
                        var total=-1;
                        var showRowCount;
                        var lengths=[];
                        var triggerDom;
                        var triggerOpen;
                        var triggerClose;
                        var length_select;
                        var maxNumbers=5;
                        var numbersContainer;
                        var url;
                        var latestFirst;
                        var tableStyle;
                        var dataStyle;              //ajax = ajax calls - //extern = ich gebe dem plugin daten und es baut die pagination
                        var getDataFrom;
                        var intervalRefresh;
                        var intervalTime;
                        var interval;
                        
                        
                        //Für alle myPaginations gleich
                        var callUrl = {
                            events: "php/getGateWayLogs.php",
                            alerts: "php/getGateWayAlerts.php"
                        };
                        

			//Set the default values, use comma to separate the settings, example:
			var defaults = {
				page: 1,
				length: 10,
                                id: -1,
                                showRowCount: "false",
                                lengths: [10,25,50,100],
                                maxNumbers: 5,
                                latestFirst: false,
                                tableStyle: "table",
                                dataStyle:"ajax",
                                intervalRefresh: false,
                                intervalTime: 5000
			};
                        
			var options =  $.extend(defaults, options);
                    
                        function init(){
                            
                            obj.addClass("mypagination");
                            
                            url=callUrl[context];
                            buildTable();
                            buildPagination();
                            //getAllData();
                            //getLatestDataPage();
                            //fillTable(0); //0 indiziert
                            
                           
                           
                           /* Add Events */
                           //Event, dass das die gesamte Tabellle erstmal mit den latestData füllt.
                           //Hier: Wenn das Modal geöffnet wird
                            $(document).on(triggerOpen,triggerDom,function(){
                                
                                console.log("myPagination EVENT : Event to start myPagination got triggerd.");
                               
                                id=$("#gatewayDetailsModal").attr("gateway-id");
                                //set select auf ersten eintrag
                                //set page auf 1
                                //set goto auf 1
                                total=-1;
                                page=1;
                                alldata=[];
                                pageCount=0;
                                //Set das Length Select auf den ersten Eintrag
                                $(".mypagination-select",obj).val($("option:first",length_select).val());
                                length=$(".mypagination-select",obj).val();
                               
                                //Rufe Event auf, um das befüllen der Tabelle anzustoßen
                                //sollte anders gemacht werden
                                //event_lengthChanged(length_select);
                               
                               if(dataStyle==="ajax"){
                                   getTotalAndPageCount();
                               } else {

                               }
                                

                                handleAction();
                                
                                if(intervalRefresh){
                                    interval=window.setInterval(intervalPagination,intervalTime);
                                }
                            
                            });
                            
                            $(document).on(triggerClose,triggerDom,function(){
                                if(intervalRefresh){
                                    window.clearInterval(interval);
                                }
                            });
                            
                            $(obj).on("click",".mypagination-button",function(){
                                event_NumberClicked($(this));
                            });
                               
                            $(obj).on("change",".mypagination-result-length .mypagination-select",function(){
                                event_lengthChanged($(this));
                            });
                            
                            $(obj).on("change",".mypagination-goto .mypagination-select", function(){
                                event_gotoNumberChanged($(this));
                            });

                            
                        }
                        
                        function intervalPagination(){
                            console.log("myPagination interval");
                            console.log(obj);
                            handleAction();
                        }
                        function getTotalAndPageCount(){
 
                            if (dataStyle==="ajax") {
                                $.ajax({
                                url: url,
                                method: "POST",
                                async: false,
                                data: {
                                    ID: id,
                                    PAGE: 1,
                                    LENGTH: 1
                                }
                                }).done(function(response){
                                    response=JSON.parse(response);
                                    total=response["total"];
                                    pageCount=Math.ceil(total/length);
                                });
                            } else if(dataStyle==="extern"){
                                var a  = getDataFrom();
                                total=a.length;
                                pageCount=Math.ceil(total/length);
                            }
                            
                        }
                        
                        function newGetData()
                        {
                            //get Daten mit gesetzter url, id, page und length
                            //schreibe in alldata und total
                            
                            //wenn das latestfirst flag gesetzt ist, dann sollen die neusten Daten zuerst angezeigt werden
                            //die neusten Daten stehen in hinteren Seiten
                            //pageCount-page = Seite, die ich haben will
                            //Bsp: PageCount =10 , click in Pagination auf 2, dann will ich Seite 9
                            var _page=page;
                                                
                            if(pageCount===0){
                                pageCount=1;
                            }
                            
                            
                            
                            //clear alldata
                            alldata=[];
                            if (dataStyle==="ajax"){
                                
                                if(latestFirst){
                                    _page=parseInt(pageCount)-parseInt(page)+1;
                                }
                                
                                $.ajax({
                                    url: url,
                                    method: "POST",
                                    async: false,
                                    data: {
                                        ID: String(id),
                                        PAGE: String(_page),
                                        LENGTH: String(length)
                                    }
                                }).done(function(response){
                                    try{
                                        response=JSON.parse(response);
                                        response[context].forEach(function(row){
                                            alldata.push(row);
                                        });
                                        total=response["total"];
                                    } catch(e){
                                        total=0;
                                    }

                                });
                            } else if(dataStyle==="extern"){
                                
                                fillAllData();
                               
                                
                                
                            }
                            
                            
                            
                        }
                        
                        
                        function fillAllData(){
                            
                            var val=getDataFrom();
                                
                            if(val!==undefined){
                                total=val.length;
                                var ende;
                                var start;
                                var _page;
                                
                                pageCount=Math.ceil(total/length);
                                
                                /*if(latestFirst){
                                    _page=parseInt(pageCount)-(parseInt(page)+1);
                                }*/
                                
                                
                                //start = (page -1)*länge
                                //bei erster seite  =  0
                                //bei zweiter seite = 10
                                //bei dritter seite = 20
                                
                                //total = anzahl elemente im array
                                //page = 1, 2, 3,...
                                //length = 5,10,...
                                
                                //wenn (page * länge) -1 > total
                                //
                                //bei erster seite  =  9    
                                //bei zweiter seite = 19
                                //bei dritter seite = 29

                                //wenn weniger einträge auf der seite, als länge der seite
                                //bsp seite 2, total 24, länge 10, dann auf seite 4 nur 4 einträge
                                //wenn total < page*länge
                                //bei erster seite  = alles was kleiner 10 ist
                                //bei zweiter seite = alles was kleiner 20 ist
                                //bei dritter seite = alles was kleiner 30 ist
                                
                                /*if((page*length)-1 > total){
                                    ende=total-1;
                                } else*/ 
                                /*
                                start=(_page-1)*length;
                                if(total< _page*length ){
                                    ende = total;
                                } else{
                                    ende=_page*length;
                                }
                                
                                for (var x = start; x<ende;x++){
                                    alldata.push(val[x]);
                                }
                                */
                                /* neu */
                                _page=parseInt(page);
                                _pageCount=parseInt(pageCount);
                                _total=parseInt(total);
                                _length=parseInt(length);
                                if(_page===1 && _page!== _pageCount){
                                    ende = _total-1;
                                    start=ende-_length+1;
                                } else if(_page===_pageCount){
                                    start=0;
                                    ende=_total- (_page-1)*_length -1;
                                }else{
                                    start=_total - _page*_length +1;
                                    ende=start+_length-1;
                                }
                                console.log("pageCount: "+_pageCount);
                                console.log("total: "+_total);
                                console.log("length: "+_length);
                                console.log("start: "+start+" ; ende: "+ende);
                                for (var x = start; x<=ende;x++){
                                    alldata.push(val[x]);
                                }
         
                                    
                            } else {
                                total=0;
                            }
        
                        }
                        
                        function getData(url, data){
                            alldata=[];
                            return $.ajax({
                                url: url,
                                method: "POST",
                                async: false,
                                data: data
                                
                                /*success:function(response){
                                    //domFunctions(JSON.parse(response));
                                    return JSON.parse(response);
                                },
                                error:function(){
                                    //alert("ajax error");
                                    return null;
                                }*/
                            });
                        }
                        
                        function getLatestDataPage(){
                            var data;
                            if (total<=0){
                                data={
                                    id:id,
                                    page:1,
                                    length:1
                                };
                                var response;
                                getData(callUrl[context],data).done( function(data){
                                    response=JSON.parse(data);
                                    total=response["total"];
                                });
                            } 
                            
                            if(total===0){
                                return;
                            }
                            
                            pageCount=Math.ceil(total/length);
                            
                            data={
                                    id:id,
                                    page:pageCount,
                                    length:length
                                };
                            
                            getData(callUrl[context],data).done( function(data){
                                response=JSON.parse(data);
                                total=response["total"];
                                response[context].forEach(function(row){
                                    alldata.push(row);
                                });
                            });
                            
                        }
                        
                        function getAllData(){
                            
                            var data={
                                id:id,
                                page:page,
                                length:length
                            };
                            var response;
                            getData(callUrl[context],data).done( function(data){
                                response=JSON.parse(data);
                                response[context].forEach(function(row){
                                    alldata.push(row);
                                });
                            });

                            pageCount=Math.ceil(response["total"]/page);
                            data={
                                    id:id,
                                    page: null,
                                    length:length
                                };
                                
                            for (var c=2;c<=pageCount;c++){
                                data.page=c.toString();
                                getData(callUrl[context],data).done( function(data){
                                    response=JSON.parse(data);
                                    response[context].forEach(function(row){
                                        alldata.push(row);
                                    });
                                });
                            }
                            alldata.sort((a,b) => (a[sortBy] > b[sortBy]) ? 1 : -1);

                            
                            
                        }
                        
                        function newFillTable()
                        {
                            if(total<=0){
                                setError("Keine Daten gefunden");
                                return ;
                            }
                            
                            //Get Container für die Rows
                            var container=$(".mypagination-content",obj);
                            
                            //leere den Container (kann auch vorher fadeOut, dann leeren und dann fadeIn machen)
                            container.empty();
                            
                            var tr,td;
                            
                            //alldata enthält nur die daten, die auch angezeigt werden soll
                            //deswegen einfache for schleife
                            
                            for (var c=0;c<alldata.length;c++){
                                tr=$("<tr />",{
                                    
                                });
                                
                                if(showRowCount==="true"){
                                    td=$("<td />", {
                                        text: c
                                    });
                                    tr.append(td);
                                }
                                
                                cols.forEach(function(col){
                                    
                                    var text="", cls="", html, attributes=[];
                                    
                                    if(col["customClass"]!==undefined){
                                        cls+=col["customClass"];
                                    }
                                    if(col["customAttr"]!==undefined){
                                        attributes=col["customAttr"];
                                        
                                    }
                                    
                                    switch(col["format"]){
                                        case "time":
                                            text=convertDateInTimeformat(alldata[c][col["data"]].toString());
                                            cls+=" w-auto text-nowrap";
                                            break;
                                        case "icon-alerts":
                                            switch(alldata[c][col["data"]]){
                                                case 2:
                                                    html="&#9888;";
                                                    break;
                                                case 3:
                                                    html="&#9940;";
                                                    break;
                                                default:
                                                    text="";
                                                    break;
                                            }
                                            break;
                                        default:
                                            if(alldata[c]!==undefined){
                                                if(col["data"]!==undefined){
                                                    text=alldata[c][col["data"]];
                                                }
                                            }
                                            
                                            break;
                                            
                                    } 
                                    
                                    td=$("<td />",{
                                        text: text,
                                        "class": cls,
                                        html: html  
                                    });
                                    
                                    attributes.forEach(function(a){
                                        td.attr(a.name,alldata[c][a.data]);
                                    });
                                    
                                    tr.append(td);
                                });
                                if(latestFirst){
                                    container.prepend(tr);
                                } else {
                                    container.append(tr);
                                }
                                   
                            }
                            
                        }
                    
                    
                        function getHtmlContent(){
                            return ;
                            
                            
                        }

                        
                        function fillTable(actPage)
                        {
                            
                            if(total<=0){
                                setError("Keine Daten gefunden");
                                return ;
                            }
                            
                            var container=$(".mypagination-content",obj);
                            $(container).empty();
                            var tr, td;
                            if (alldata.length>length){
                                for(var c=length*actPage;c<(length*actPage)+length;c++){
                                tr=$("<tr />");
                                cols.forEach(function(col){
                                    
                                    if(showRowCount==="true"){
                                        td=$("<td />", {
                                            text: c
                                        });
                                        tr.append(td);
                                    }
                                    td=$("<td />",{
                                        text: (alldata[c])[col["data"]]
                                        //text: row[col["data"]]
                                    });
                                    
                                    tr.append(td);
                                });
                                container.prepend(tr);
                                }
                            } else {
                                for(var c=0;c<alldata.length;c++){
                                tr=$("<tr />");
                                if(showRowCount==="true"){
                                        td=$("<td />", {
                                            text: c
                                        });
                                        tr.append(td);
                                    }
                                cols.forEach(function(col){
                                    td=$("<td />",{
                                        text: (alldata[c])[col["data"]]
                                        //text: row[col["data"]]
                                    });
                                    
                                    tr.append(td);
                                });
                                container.prepend(tr);
                            }
                            }
                            
                            
                            
                            
                            //welche daten
                            /*$("thead > tr", des).forEach(function(i){
                                
                            });
                            
                            response[context].forEach(function(i){
                                
                            });*/
                            
                            
                        }
                    
                        function buildTable(){
                            
                            obj.addClass("table-responsive");
                            
                            /* create table */
                            var table=$("<table />",{
                               "class":"table table-striped" 
                            });
                            
                            /* create thead */
                            var thead=$("<thead >/", {
                                "class": "table-dark"
                            });
                            
                            /*create tr */
                            var tr=$("<tr />");
                            
                            var th;
                            /* create mehrere ths und füge sie tr hinzu */
                            
                            /* 12 is max
                             * wenn rowcount, dann -1
                             *      11 oder 12
                             * 
                             */
                            
                            if (showRowCount==="true"){
                                    th=$("<th />",{
                                        text:"#",
                                        "mypagination-data-context": "#count",
                                        "class": ""
                                    });
                                    tr.append(th);
                                }
                            if ( cols!==undefined){
                                cols.forEach(function(col){
                                    var cls="";
                                    if(col["customClass"]!==undefined){
                                        cls+=col["customClass"];
                                    }

                                    th=$("<th />",{
                                        text: col["title"],
                                        "mypagination-data-context": col["data"],
                                        "class": "mypagination-column-title "+cls
                                    });
                                    tr.append(th);
                                });
                            }
                            
                            
                            /*create tbody */
                            var tbody=$("<tbody \>",{
                                "class":"mypagination-content"
                            });
                            
                            /*füge thead tr hinzu */
                            thead.append(tr);
                            /* füge der tabe thead hinzu*/
                            table.append(thead);
                            /* für der table tbody hinzu*/
                            table.append(tbody);
                            
                            
                            /* für dem mypagination container die table hinzu */
                            obj.append(table);
                        }
                    
                        function buildPagination(){
                            
                            /*create pagination div*/
                            var pagination_div=$("<div />",{
                               
                            });
                            
                            /*create length div*/
                            var length_div=$("<div />",{
                                "class":"mypagination-result-length"
                            });
                            
                            /*create select */
                            length_select=$("<select />",{
                               "class":"mypagination-select"
                            });
                            
                            var option;
                            lengths.forEach(function(i){
                                option=$("<option />",{
                                    text:i
                                });
                                length_select.append(option);
                            });
                            
                            length_div.append(length_select);
                            pagination_div.append(length_div);
                            
                            
                            
                            var numbers_div;            
                            if(pageCount<2){
                                numbers_div=$("<div />",{
                                    "class":"mypagination-numbers mypagination-hide"
                                });
                                
                            } else {
                                
                                numbers_div=$("<div />",{
                                    "class":"mypagination-numbers"
                                });                             
                            }
                            
                            var prev=$("<a />",{
                                    "class":"mypagination-button mypagination-prev mypagination-disabled",
                                    text:"Prev",
                                    "index":"-1"
                                });
                            
                            numbersContainer=$("<span />",{
                                "class": "mypagination-numbers-container"
                            });
                            var number_button;
                            
                            number_button=$("<a />",{
                                    "class":"mypagination-button pagination-active",
                                    text:"1",
                                    "index":"1"
                                });
                            numbersContainer.append(number_button);
                            
                            var goto;
                            if(pageCount<5){
                                for(var c=2;c<=pageCount;c++){
                                    number_button=$("<a />",{
                                        "class":"mypagination-button",
                                        text:c,
                                        "index":c
                                    });
                                    numbersContainer.append(number_button);
                                }
                                goto=$("<div />", {
                                   "class":"mypagination-goto mypagination-hide" 
                                });
                            } else {
                                for(var c=2;c<=5;c++){
                                    number_button=$("<a />",{
                                        "class":"mypagination-button",
                                        text:c,
                                        "index":c
                                    });
                                    numbersContainer.append(number_button);
                                }
                                goto=$("<div />", {
                                   "class":"mypagination-goto" 
                                });
                            }
                            
                            
                            
                            
                            
                            
                            var next=$("<a />",{
                                "class":"mypagination-button mypagination-next",
                                text:"Next",
                                "index":"+1"
                            });
                            
                            var goto_title=$("<span />",{
                               text:"Goto:" 
                            });
                            goto.append(goto_title);
                            var goto_select=$("<select />",{
                                "class": "mypagination-select"
                            });
                            
                            var opt;
                            for(var c=1;c<=pageCount;c++)
                            {
                                opt=$("<option />",{
                                    text: c
                                });
                                goto_select.append(opt);
                            }
                            goto.append(goto_select);
                            
                            numbers_div.append(goto);
                            
                            numbers_div.append(prev);
                            numbers_div.append(numbersContainer);
                            numbers_div.append(next);
                            pagination_div.append(numbers_div);
                            
                            obj.prepend(pagination_div);
                            
                            
                            
                            //
                            
                            
                        }
                    
                    
                        function refreshNumbers(){
                            
                            //page ist die aktuelle page
                            
                            //numbersContainer.fadeOut();
                            numbersContainer.empty();
                            var number;
                            var numberClass="mypagination-button";
                            var numberActive="mypagination-active";
                            var disabled="mypagination-disabled";
                            var hide="mypagination-hide";
                            
                            
                             
                             pageCount=Math.ceil(total/length);
                             
                            if(pageCount<=1){
                                $(".mypagination-numbers",obj).fadeOut();
                                return;
                            } else {
                                $(".mypagination-numbers",obj).fadeIn();
                            }
                            
                            //de-/aktiviere prev bzw next
                            if(page===1){
                                $(".mypagination-numbers .mypagination-prev",obj).addClass(disabled);
                            } else {
                                $(".mypagination-numbers .mypagination-prev",obj).removeClass(disabled);
                            }
                            
                            if(page===pageCount){
                                $(".mypagination-numbers .mypagination-next",obj).addClass(disabled);
                            } else {
                                $(".mypagination-numbers .mypagination-next",obj).removeClass(disabled);
                            }
                            
                            //aktive page hinzufügen
                            /*number=$("<a />",{
                                "class": numberClass+" "+numberActive,
                                text: page
                            });*/
                            //numbersContainer.append(number);
                            
                            //wenn die page 1 ist, dann ganz links active
                            //wenn page = max ist, dann dann ganz rechts
                            //wenn page =2, dann das zweite aktiv
                            //wenn page das zweitletzte, dann das 2. letzte aktiv
                            
                            if(page===1){
                                
                            }
                            
                            if (pageCount < maxNumbers){
                                //Nummern hnizufügen 
                                for (var c=1;c<=pageCount;c++){
                                    if(c===page){
                                        number=$("<a />",{
                                            "class": numberClass+" "+numberActive,
                                            text: c
                                        });
                                    } else {
                                        number=$("<a />",{
                                            "class": numberClass,
                                            text: c
                                        });
                                    }
                                    numbersContainer.append(number);
                                }
                               
                                
                            } else {
                                // 1 und 2 beginnen bei 1 anzuzählen
                                if(page-Math.floor(maxNumbers/2)<1)
                                {
                                    
                                    for (var c=1;c<=maxNumbers;c++){
                                        if(c===page){ 
                                            number=$("<a />",{
                                                "class": numberClass+" "+numberActive,
                                                text: c
                                            });
                                        } else {
                                            number=$("<a />",{
                                                "class": numberClass,
                                                text: c
                                            });
                                        }
                                        numbersContainer.append(number);
                                    }
                                } else if(page+Math.floor(maxNumbers/2)>pageCount){
                                    
                                    for (var c=pageCount-maxNumbers+1;c<=pageCount;c++){
                                        if(c===page){ 
                                            number=$("<a />",{
                                                "class": numberClass+" "+numberActive,
                                                text: c
                                            });
                                        } else {
                                            number=$("<a />",{
                                                "class": numberClass,
                                                text: c
                                            });
                                        }
                                        numbersContainer.append(number);
                                    }
                                    
                                }else {
                                    for (var c=page-Math.floor(maxNumbers/2);c<=page+Math.floor(maxNumbers/2);c++){
                                        if(c===page){ 
                                            number=$("<a />",{
                                                "class": numberClass+" "+numberActive,
                                                text: c
                                            });
                                        } else {
                                            number=$("<a />",{
                                                "class": numberClass,
                                                text: c
                                            });
                                        }
                                        numbersContainer.append(number);
                                    }
                                }
                                
                                //nur 5 nummern hinzufügen
                                
                                
                            }
                            
                             //numbersContainer.fadeIn();
                            
                        }
                    
                        function setError(text){
                            
                            var container=$(".mypagination-content",obj);
                            $(container).fadeOut();
                            $(container).empty();
                            var tr=$("<tr />",{
                                
                            });
                            var col_length=cols.length;
                            if (showRowCount==="true"){
                                col_length+=1;
                            }
                            var td=$("<td />",{
                                text: text,
                                "colspan": col_length,
                                "class": "mypagination-error"
                            });
                            tr.append(td);
                            container.append(tr);
                            $(container).fadeIn();
                        }
                    
                        function handleAction(){
                            //Funktion, die eigentlich immer aufgerufen wird
                            //Holt Daten -> Parameter (page, length) werden woanders gesetzt
                            //Refresht Numbers
                            //Refresht Goto
                            
                            //console.log("handleAction ");
                            //console.log(obj);
                            
                            //Hole neue Daten
                            newGetData();

                            refreshNumbers();
                            refreshGoto();
                            
                            //dann filltable mit den Daten
                            newFillTable();
                            
                            
                            
                        }
                    
                    
                        function refreshGoto(){
                                                
                            var goto=$(".mypagination-goto .mypagination-select",obj);
                            var option;
                            
                            goto.empty();
                            
                            if(pageCount>0){
                               goto.fadeIn();
                               for (var c=1;c<=pageCount;c++){
                                    option=$("<option />", {
                                        text: c
                                    });
                                    goto.append(option);
                                }
                                //set aktuelle Page als selected
                                $(goto).val(page);
                            } else {
                                goto.fadeOut();
                            }              
                        }
                    
                        function event_NumberClicked(element){
                            
                            //event kommt rein, ich setze nur die page rufe handleAction auf
                            //handleAction holt Daten
                            
                            if(element.text()==="Prev"){
                                if(page!==1){
                                   page-=1; 
                                }
                            } else if(element.text()==="Next"){
                                if(page!==pageCount){
                                   page+=1; 
                                }
                            } else {
                                page=parseInt(element.text());
                            } 
                            
                            handleAction();
                        }
                    
                        function event_lengthChanged(element)
                        {
                            // neue Abfrage machen
                            
                            var lengthselection=$(".mypagination-result-length .mypagination-select",obj);
                            length=parseInt(lengthselection.val());
                            page=1;
                            handleAction();
                            return;
                            
                            length=len;
                            getLatestDataPage();
                            fillTable(0);
                            
                            refreshNumbers();
                            

                        }
                        
                        function event_gotoNumberChanged(element){
                            
                            page=parseInt(element.val());
                            handleAction();
                            
                        }
                    
                    var myPagination={
                        refreshTable:function(){
                            handleAction();
                        }
                    };
                    this.each(function() {
                                obj = $(this);
                                //id=options["id"];
                                
                                context=options["context"];
                                cols=options["cols"];
                                sortBy=options["sortBy"];
                                showRowCount=options["showRowCount"];
                                lengths=options["lengths"];
                                length=lengths[0];
                                triggerDom=options["triggerDom"];
                                triggerOpen=options["triggerOpen"];
                                triggerClose=options["triggerClose"];
                                latestFirst=options["latestFirst"];
                                dataStyle=options["dataStyle"];
                                getDataFrom=options["getDataFrom"];
                                intervalRefresh=options["intervalRefresh"];
                                intervalTime=options["intervalTime"];
                                
                                /*var length_select= $(".mypagination-result-length .mypagination-select", obj)[0];
                                var length=$(":selected",length_select)[0].text;*/
                                init();
                               
    		});
                
    		return myPagination;
    	}
    });
})(jQuery);



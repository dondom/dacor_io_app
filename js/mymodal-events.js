/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var datastreamPagination;
var alertsPagination;
var logsPagination;
var devicePagination;

$(document).ready(function(){
    
     $('#gatewayDetailsModal').on('shown.bs.modal',function(){
    //Map Layer mit Standort-Punkt hinzufügen
        gMap = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([gLon,gLat ]),
                zoom: 14
            })
        });
        gMap.addLayer(gLayer);

    });
    
    //Event - Modal wird geschlossen
    $('#gatewayDetailsModal').on("hidden.bs.modal",function(){
    //Interval für die stätige Funktionsausführung löschen - im Moment nicht genutzt
               
        //Entferne Layer von der Map, weil sonst alle Layers übereinander lägen
        gMap.removeLayer(gLayer);
        $(gMapSelector).children().remove();
        //myWebsocketClose();
        
        try{
            gMyWebsocket.close();
        } catch(e){
            
        }
        
    });
    
    alertsPagination=$(".mypagination-alerts").myPagination({
        cols: [{
            title: "",
            data: "alertId",
            format: "icon-alerts",
            customClass: "mypaginaiton-alertIcon"
        },{
            title: "Timestamp",
            data: "ts",
            format: "time"
        },{
            title: "Message",
            data: "alert",
            format: "string"
        }],
        context: "alerts",
        latestFirst: true,
        sortBy: "ts",
        showRowCount: "false",
        triggerDom: "#gatewayDetailsModal",
        triggerOpen: "shown.bs.modal",
        triggerClose: "hidden.bs.modal",
        intervalRefresh: false,
        intervalTime: 10000
    });
            
    logsPagination=$(".mypagination-logs").myPagination({
        cols: [{
            title: "TimeStamp",
            data: "ts",
            format: "time"
        },{
            title: "Event",
            data: "evt",
            customClass: "sev",
            customAttr: [{
                name: "sev-num",
                data: "sev"
            }]
        },{
            title: "Message",
            data: "msg"
        }],
        lengths: [10,25,50,100],
        latestFirst: false,
        context: "events",
        triggerDom :"#gatewayDetailsModal",
        triggerOpen: "shown.bs.modal",
        intervalRefresh: false,
        intervalTime: 5000
    });
    
    datastreamPagination=$(".mypagination-datastream").myPagination({
        cols: [{
            title: "TS",
            data: "ts",
            format: "time"
        },{
            title: "CmD",
            data: 'cmd'
        },{
            title: "DevAddr",
            data: 'devaddr'
        },{
            title: "MIC",
            data: 'mic'
        },{
            title: "FCNT",
            data: 'fcnt'
        },{
            title: "Len",
            data: 'len'
        },{
            title: "Time",
            data: 'time',
            format: "time"
        },{
            title: "SF",
            data: 'sf'
        },{
            title: "RSSI",
            data: 'rssi'
        },{
            title: "SNR",
            data: 'snr'
        },{
            title: "TOA",
            data: 'toa'
        },{
            title: "BW",
            data: 'bw'
        },{
            title: "FCTRL",
            data: 'fctrl'
        },{
            title: "Port",
            data: 'port'
        },{
            title: "Encoded Paket",
            data: 'data'
        }],
        lengths: [5,10,25,50,100],
        latestFirst: true,
        triggerDom :"#gatewayDetailsModal",
        triggerOpen: "shown.bs.modal",
        tableStyle: "table-sm",
        dataStyle: "extern",
        getDataFrom: function(){
            if(gMyWebsocket!==undefined){
                return gMyWebsocket.getMessages();
            } else {
                return undefined;
            }
        }
    });
    
    devicePagination=$(".mypagination-devices").myPagination();
    
    $("#details-accordion").accordion({
        heightStyle: "content",
        collapsible: true,
        activate: function(e,ui){
            accordionChanged(e,ui);
        }
    });
    
    $("#data-stream .accordion-title-number-txt.mywebsocket-message-new-subscriber").on("mywebsocket.message.new.handler",function(e, c, tc, evt){
        
        console.log("notification count");
        
        var count=gMyWebsocket.getTempCount();
        
        var fadein=$(this).attr("fadeIn");
        
        if(fadein==="true"){
            $(this).fadeIn(200);
            $(this).attr("fadeIn","isShown");
        }
        
        //hier könnte ich theoretisch alles rüberholen um die tabele zu füllen
        $(this).text(count);
        datastreamPagination.refreshTable();
    });
    
    $(".mywebsocket.mywebsocket-message-new-subscriber").on("mywebsocket.message.new.handler",function(e, c, tc, evt){
        
        console.log("count: "+c+", tempCount: "+tc+", evt: "+evt);
        
        console.log(gMyWebsocket.getMessages());
        
        //hier refresh von Pagination ausführen;
        datastreamPagination.refreshTable();
        
        
        
        
    });
    
    $(".accordion-title-number").on("mywebsocket.status.changed.handler",function(e,status){

        console.log("myWebsocket HANDLER : Status Changes : "+status);

        if(status==="opened"){
            $(".accordion-title-number-button-container .icon-play").fadeOut(200,function(){
                $(".accordion-title-number-button-container .icon-refresh").fadeIn(200);
                $(".accordion-title-number-button-container .icon-stop").fadeIn(200);
            });
        } else if(status==="closed"){
            $(".accordion-title-number-button-container .icon-stop").fadeOut(200);
            $(".accordion-title-number-button-container .icon-refresh").fadeOut(200, function(){
                $(".accordion-title-number-button-container .icon-play").fadeIn(200);
            });   
        } 
    });
    
    $(".accordion-title-number-button-container .accordion-title-number-button").on("click",function(e){
        e.stopPropagation();
        e.stopImmediatePropagation();
        var action = $(this).attr("action");
        console.log("clicked: "+action);
        if(action==="websocket-start"){
            gMyWebsocket.open();
        } else if(action==="websocket-stop"){
            gMyWebsocket.close();
        }
    });
    
    
    
    
    $(document).on("click",".stopInterval",function(){
        alert("interval gestoppt");
        window.clearInterval(gModalInterval);
    });
    
});
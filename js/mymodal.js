/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 var gModalDetails, gLat, gLon, gModalSelector, gLayer, gMap, gMapSelector="#map", gGatewayId, gGatewayToken, gModalInterval, gMyWebsocket;
 
function myModalStart(modalSelector,id){
    
    gGatewayId=id;
    
    getGatewayInformation().done(function(respone){
        gModalDetails=JSON.parse(respone);
    });
    
    //parse details string in json object
    gModalSelector=modalSelector;
    
    //Fill Static Data
    staticData();
    
    
    var intervalTime=2000;
    //intervall Execution for values that get updated
    gModalInterval=window.setInterval(intervalExec,intervalTime);
    
    intervalExec();
    
    specialFunctions();
    
    $(gModalSelector).modal();
    
}

function getGatewayInformation(){
                
               return $.ajax({
                    url: "php/getGatewayDetails.php",
                    method: "POST",
                    async: false,
                    data: {id: gGatewayId}
                });
}

function specialFunctions(){
    $("#gatewayDetailsModal").attr("gateway-id",gModalDetails["_id"]);
    $("#gatewayDetailsModal").attr("gateway-token",gModalDetails["token"]);
    gGatewayId=gModalDetails["_id"];
    gGatewayToken=gModalDetails["token"];
    
    initMyWebsocket();
 
    
    function initMyWebsocket(){
        gMyWebsocket=new myWebsocket_new(gModalDetails["_id"],gModalDetails["token"]);
        gMyWebsocket.open();
    }
    
}

function accordionChanged(e,ui){
    
    //Alle Funktionen, die ausgeführt werden sollen, wenn ein anderer Tab von dem Accordion geöffnet wird
    var h=ui.newHeader;
    var title=$(".accordion-title",h).text();
    if(title==="Data Stream"){
        //wenn der Tab "Data Stream" geööfnet wird
        //myWebsocket();
        //$(".accordion-title-number-txt#data-stream").fadeOut();
        
        //Blende Notification aus
        
        $(".accordion-title-number#data-stream .accordion-title-number-txt").fadeOut(100);
        $(".accordion-title-number#data-stream .accordion-title-number-txt").attr("fadeIn","false");
        
        //enterne das fadein atrribut
        $("#data-stream .accordion-title-number-txt").attr("fadeIn","false");
        $("#data-stream .accordion-title-number-txt").fadeOut(100);
    } else {
        //myWebsocketClose();
        console.log("accordion event zu");
        //Blende Notification ein und setze den Notification Count auf 0
        // $(".accordion-title-number-txt#data-stream").fadeIn();
        
        //setze den tmpCounter auf 0, füge das fadeIn attribut hinzu
        //accordion-title-number-txt mywebsocket-message-subscriber
        
        $("#data-stream .accordion-title-number-txt").attr("fadeIn","true");
        gMyWebsocket.setTempCount(0);
        ;$(".accordion-title-number#data-stream .accordion-title-number-txt").attr("fadeIn","true");
    }
}

function addEvents(){
     
    
}

function intervalExec(){
    
    //Alles was kontant ausgeführt werden soll hier hinein
    
    //GatewayListe: wäre eigentlich schön, wenn die Liste sich auch updatet
    
    //Im Modal: Online Check! Latency, Last Data, Last Connect, Last Keep Alive
    //Alerts+Logs: Das hätte ich gerne so, dass im h3 vom Accordion die Anzahl der neuen Alerts steht
    // Update Alerts: Auch sätig updaten. Die Struktur dafür ist schon da.Stätig abfragen, wenn sich total geändert hat, dann alles neu bauen
    //Data Stream: startet erst wenn man das Accordion dafür öffnet. Könnte natürlich auch direkt beim Modal Open starten. Und dann auch Anzahl der Nachrichten anzeigen
                    //Dann aber Table mit fixed header und einfach komplett füllen lassen
    
    //console.log("intervallExec");
    
    getGatewayInformation().done(function(respone){
        gModalDetails=JSON.parse(respone);
    });
    
    
    onlineStatus();
    latencyStatus();
    lastDataStatus();
    lastConnectStatus();
    lastKeepAliveStatus();
    remotetimeOffset();
}


function staticData(){
    //Image + Version
    modalImageAndVersion();
    //Title
    modalTitle();
    //Configuration
    modalConfiguration();
    //Details
    detailsContainer();
    //mehr Details
    mehrDetailsContainer();
    //Aktionen
    aktionenContainer();
}
function modalImageAndVersion(){
    //selectors
    var img             = $(".mymodal-image");
    var basename        = $(".basemodel .basename");
    var modelname       = $(".basemodel .modelname");
    var version         = $(".version");
    
    //values
    var basenameVal      = gModalDetails["base"];
    var modelnameVal     = gModalDetails["model"];
    var versionVal       = gModalDetails["version"];
    
    //src string
    var src="img/modele/"+basenameVal+"-"+modelnameVal+".png";
    
    //set values
    img.attr("src",src);
    img.attr("alt",src);
    basename.text(basenameVal);
    modelname.text(modelnameVal);
    version.text(versionVal);

}

function modalTitle(){
    //selectors
    var title   = $(".mymodal-title .mymodal-title-txt");
    
    //values
    var text    = gModalDetails["title"];
    
    //set values
    title.text(text);
}

function modalConfiguration(){
    //selectors
    var communityAccess         =$(".details-configurations .community .value");
    var alertsNotifications     =$(".details-configurations .alerts .value");
    var ignoreData              =$(".details-configurations .ignore .value");
    
    //values
    var communityAccessFlag     =gModalDetails["open"];
    var alertsNotificationsFlag =gModalDetails["alerts"];
    var ignoreDataFlag          =gModalDetails["ignore"];
    
    //add to array to do it for all selectors
    var array=[];
    array.push([communityAccess,communityAccessFlag]);
    array.push([alertsNotifications,alertsNotificationsFlag]);
    array.push([ignoreData,ignoreDataFlag]);
    
    //iterate through array & set values
    for (var c = 0; c<array.length; c++)
    {
        if (array[c][1]===true){
            array[c][0].html("&#10004;");
            array[c][0].addClass("checked");
            array[c][0].removeClass("notChecked");
        } else {
            array[c][0].html("&#10006;");
            array[c][0].removeClass("checked");
            array[c][0].addClass("notChecked");
        }
    }
    
}

function detailsContainer(){
  
    eui();
    mac();
    connectedIp();
    roaming();
    networks();
    location();
    mapLocation();
    
    function eui(){
        //selectors
        var eui=$(".eui-value");
        
        //values
        var text=gModalDetails["EUI"];
        
        //set values
        eui.text(text);
        
    }
    
    function mac(){
        //selectors
        var mac=$(".mac-value");
        
        //values
        var text=gModalDetails["MAC"];
        
        //set values
        mac.text(text);
    }
    
    function connectedIp() {
        //selectors
        var ip=$(".ip-value");
        
        //values
        var text=gModalDetails["ip"];
        
        //set values
        ip.text(text);
    }
    
    function roaming() {
        //selectors
        var roaming=$(".roaming-value");
        
        //values
        var text=gModalDetails["roamingId"];
        
        //set values
        roaming.text(text);
    }
    
    function networks() {
        //selectors
        var networks = $(".networks-value");
        
        //values
        var networksVal=gModalDetails["network"];
        var usedText="&#10004;";
        var notUsedText="&#10006;";
        var used;
        var networkCount=0;
        var name;
        var ip;
        
        //new element containers
        var usedSpan;
        var countSpan;
        var nameSpan;
        var ipSpan;
        var networkDiv;
        
        //clear container
        networks.empty();
        
        for (var c=0; c<networksVal.length; c++){
            
            //set values
            used=networksVal[c]["used"];
            networkCount=c+1;
            name=networksVal[c]["name"];
            ip=networksVal[c]["ip"];
            
            //create elements
            usedSpan=$("<span />");
            countSpan=$("<span />");
            nameSpan=$("<span />");
            ipSpan=$("<span />");
            networkDiv=$("<div />");
            
            //set values in elements
            used = (used) ? usedText : notUsedText;
            usedSpan.html(used); 
            countSpan.text("Interface #"+networkCount+" ");
            nameSpan.text(name+" ");
            ipSpan.text(ip);
            
            //add elements to dom
            networkDiv.append(usedSpan);
            networkDiv.append(countSpan);
            networkDiv.append(nameSpan);
            networkDiv.append(ipSpan);
            networks.append(networkDiv);
        }
        
    }
    
    function location() {
        //selectors
        var address=$(".address-value");
        var zip=$(".zip-value");
        var city=$(".city-value");
        var country=$(".country-value");
        var lat=$(".lat-value");
        var lon=$(".lon-value");
        
        //values
        var addressVal=gModalDetails["location"]["address"];
        var zipVal=gModalDetails["location"]["zip"];
        var cityVal=gModalDetails["location"]["city"];
        var countryVal=gModalDetails["location"]["country"];
        var latVal=gModalDetails["location"]["lat"];
        var lonVal=gModalDetails["location"]["lon"];
        
        //set values
        address.text(addressVal);
        zip.text(zipVal);
        city.text(cityVal);
        country.text(countryVal);
        lat.text(latVal);
        lon.text(lonVal);
    }
    
    function mapLocation(){
        //values
        var lon=gModalDetails["location"]["lon"];
        var lat=gModalDetails["location"]["lat"];
        
        //set global vars for map
        gLon=lon;
        gLat=lat;
        
        //create new layer
        gLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [
                    new ol.Feature({
                        geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat]))
                    })
                ]
            })
        });
        
    }
    
}

function mehrDetailsContainer(){
    
    hardware();
    antennas();
    machineInfos();
    
    function hardware() {
        //selectors
        var concentrator = $(".concentrator-value");
        var bus = $(".bus-value");
        var card = $(".card-value");
        var sshtunnelservice=$(".sshtunnelserviceserver-value");
        
        
        //values
        var conncentratorVal=gModalDetails["concentratorname"];
        var busValue=gModalDetails["busname"];
        var cardVal=gModalDetails["cardname"];
        var sshtunnselserviceVal=gModalDetails["sshTunnelServiceServer"];
     
        
        //set values
        concentrator.text(conncentratorVal);
        bus.text(busValue);
        card.text(cardVal);
        sshtunnelservice.text(sshtunnselserviceVal);
        

        
        
    }
    
    function antennas() {
        //selectors
        var region = $(".region-value");
        var radioband=$(".radioband-value");
        var maxantennas=$(".maxantennas-value");
        var maxchannels=$(".maxchannels-value");
        var maxchannelplans=$(".maxchannelplans-value");
        
        //values
        var regionVal=gModalDetails["region"];
        var radiobandVal=gModalDetails["radioband"];
        var maxantennsVal=gModalDetails["maxAntennas"];
        var maxchannelsVal=gModalDetails["maxChannels"];
        var maxchannelplansVal=gModalDetails["maxChannelPlans"];
        var radiobandname;
        
        //set values
        region.text(regionVal);
        maxantennas.text(maxantennsVal);
        maxchannels.text(maxchannelsVal);
        maxchannelplans.text(maxchannelplansVal);
        
        //clear container
        radioband.empty();
        
        //element container
        var radiobandDiv;
        
        //iterate trough array
        for (var c=0; c<radiobandVal.length; c++){
            //get values
            radiobandname=radiobandVal[c];
            
            //create element
            radiobandDiv=$("<div />");
            
            //set value for element
            radiobandDiv.text(radiobandname);
            
            //add element to dom
            radioband.append(radiobandDiv);
            
        }
        
    }
    
    function machineInfos() {
        //selectors
        var machine=$(".machine-value");
        var machinename=$(".machine-name-value");
        var machinerelease=$(".machine-release-value");
        var machinesys=$(".machine-sys-value");
        var macineversion=$(".machine-version-value");
        
        //values
        var info=gModalDetails["info"];

        if(info){
            var machineVal=gModalDetails["info"]["machine"];
            var machinenameVal=gModalDetails["info"]["name"];
            var machinereleaseVal=gModalDetails["info"]["release"];
            var machinesysVal=gModalDetails["info"]["sys"];
            var machineversionVal=gModalDetails["info"]["version"];
            
            //set values
            machine.text(machineVal);
            machinename.text(machinenameVal);
            machinerelease.text(machinereleaseVal);
            machinesys.text(machinesysVal);
            macineversion.text(machineversionVal);
        }
    }
    
}

function aktionenContainer(){
    
}

function onlineStatus(){
    //selector
    var connected       = $(".mymodal-connected");
    var circle          = $(".mymodal-connected-circle");
    var text            = $(".mymodal-connect-text");
    
    //values
    var connectedFlag   = gModalDetails["connected"];
    var onlineClass     = "online";
    var offlineClass    = "offline";
    var onlineText      = "Online";
    var offlineText     = "Offline";
    
    //set values
    if(connectedFlag){
        connected.removeClass(offlineClass);
        circle.removeClass(offlineClass);
        connected.addClass(onlineClass);
        circle.addClass(onlineClass);
        text.text(onlineText);
    } else {
        connected.removeClass(onlineClass);
        circle.removeClass(onlineClass);
        connected.addClass(offlineClass);
        circle.addClass(offlineClass);
        text.text(offlineText);
    }
    
}

function latencyStatus(){
    //selectors
    var latency = $(".latency-value");
    
    //values
    var text = gModalDetails["rtt"];
    var notDefinedText="-";
    
    //set values
    if(text === undefined){
        text=notDefinedText;
    } 
    latency.text(text);
    
}

function lastDataStatus(){
    
    //selectors
    var time=$(".last-data-value");
    var date=$(".last-data-value-date");
    
    
    //values
    var dateString=gModalDetails["lastData"];
    console.log(convertDateInTimeformat(dateString).timeDiffToNow());
    
    var myDate=convertDateInTimeformat(dateString);
    
    var dateVal=myDate.toString();
    var timeVal=myDate.timeDiffToNow();

    
    //set values
    time.text(timeVal);
    date.text(dateVal); 
      
}

function lastConnectStatus(){
    //selectors
    var time=$(".last-connect-value");
    var date=$(".last-connect-value-date");
    
    //values
    var dateString=gModalDetails["lastStarted"];
    var myDate=convertDateInTimeformat(dateString);
    var timeVal=myDate.timeDiffToNow();
    var dateVal=myDate.toString();
    
    //set values
    time.text(timeVal);
    date.text(dateVal);
    
}

function lastKeepAliveStatus(){
    //selectors
    var time=$(".last-pong-value");
    var date=$(".last-pong-value-date");
    
    //values
    var dateString=gModalDetails["lastPong"];
    var myDate=convertDateInTimeformat(dateString);
    var timeVal=myDate.timeDiffToNow();
    var dateVal=myDate.toString();
    
    //set values
    time.text(timeVal);
    date.text(dateVal);
    
}

function remotetimeOffset()
{
    //selectors
    var remotetimeoffset=$(".remotetimeoffset-value");
    
    //values
    var remotetime=gModalDetails["rtime"];
    var remotetime1=gModalDetails["rtimel"];
    var resulttime=Math.round(remotetime-remotetime1)/1000;
    
    //set values
    remotetimeoffset.text(resulttime);
}
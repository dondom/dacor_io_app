/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function convertDateInTimeformat(_date){

  var myDate={
    dateObj:    null,
    hour:       null,
    minutes:    null,
    seconds: 	null,
    day: 	null,
    month: 	null,
    year: 	null,
    init: function(){
        
        if (this.dateObj===null){
            this.setDate(_date);
        }
        
        this.hour   =this.dateObj.getHours();
        this.minutes=this.dateObj.getMinutes();
        this.seconds=this.dateObj.getSeconds();
        this.day    =this.dateObj.getDate();
        this.month  =(this.dateObj.getMonth()+1);
        this.year   =this.dateObj.getFullYear();
        
    },
    toString: function(){
        var uno=this.time();
        var dos=this.date();
        var tres=uno+" "+dos;
    	return tres;
    },
    time: function(){
        var uno=this.format(this.hour);
        var dos=this.format(this.minutes);
        var tres=this.format(this.seconds);
        var cuatro=uno+":"+dos+":"+tres;
    	return cuatro;
    },
    date: function(){
        var uno=this.format(this.day);
        var dos=this.format(this.month);
        var tres=this.year.toString();
        var cuatro=uno+"."+dos+"."+tres;
    	return cuatro;
    },
    format: function(x){
        var uno=x.toString();
        var dos=uno.length;
        var tres=(dos<2) ? "0"+uno : uno;
    	return tres; 
    },
    timeDiffToNow: function(){
  
        var now=Date.now();
        var then=Date.parse(this.dateObj);//(typeof _date==="string") ? Date.parse(_date) : _date;
        
        
        if(now-then<0){
            return "0 sec ago";
        }
        
        var diff=new Date(now-then);
        
        var y,m,d,h,min,s;
        
        y   =diff.getFullYear();
        m   =(diff.getMonth()+1);
        d   =diff.getDate();
        h   =diff.getHours();
        min =diff.getMinutes();
        s   =diff.getSeconds();
        
        var uno=y;
        var dos=m;
        var tres=d;
        var cuatro=d;
        var cinco=h;
        var seis=min;
        var siete=s;
        
        uno-=1970;
        dos-=1;
        tres=(tres-1) /7;
        tres=Math.ceil(tres);
        cuatro-=1;
        cinco-=1;
        
        console.log("year:"+uno+", monat:"+dos+", tag:"+tres+", hour:"+cuatro+", mins:"+cinco+", secs:"+siete);

        
        if(uno>0)
        {  
            return "~"+uno+" year(s) ago";
            
        } else if(dos>0){
            return "~"+dos+" month(s) ago";
            
        } else if(tres>0){
            return "~"+tres+" week(s) ago";
            
        } else if(cuatro >0){
            return "~"+cuatro+" days(s) ago";
            
        } else if(cinco>0){
            return "~"+cinco+" h(s) ago";
            
        } else if(seis>0){
            return "~"+seis+" min(s) ago";
            
        } else {
            return "~"+siete+" sec ago";
        }
        
        //return this.toString();
    },
    setDate: function(x){
        var uno=x;
        try{   
            switch(typeof uno){
                case "string":     
                    if(uno.match(/^-?\d+$/)){
                        this.dateObj=new Date(parseInt(uno));

                     } else {
                         this.dateObj=new Date(uno);
                     }
                    break;

                case "number":
                    this.dateObj=new Date(parseInt(uno));
            }
        }catch(e){
            console.log("mytime.js TimeConvertError:\n"+e+"\nDate.now() is taken");
            this.dateObj=Date.now();
        }
        
    }
  };
  
    myDate.init();
  
  return myDate;
  
}
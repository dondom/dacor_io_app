/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class myWebsocket_new{
    
    id;
    token;
    count;
    tempCount;
    url;
    websocketObj;
    messages=[];
    
    
    constructor(_id,_token){
        if(_id!==undefined){
            this.id=_id;
        }
        if(_token!==undefined){
            this.token=_token;
        }
        this.count=0;
        this.tempCount=0;
        this.init(this.id,this.token);
    }
    
    init(_id, _token){
       
        if(_id!==undefined){
            this.id=_id;
        }
        
        if(_token!==undefined){
            this.token=_token;
        }
        if(this.id!==undefined && this.token !== undefined){
            this.url="wss://loriot.dacor.de/home/gwtap?gw="+this.id+"&token="+this.token;
        } else {
            console.log("myWebsocket ERROR : Could not initialize WebSocket. ID or Token is not set!");
            return;
        }
        
    }

    open(){
        var _this=this;
        if(this.websocketObj===undefined){
            try{
            
                this.websocketObj= new WebSocket(this.url);

                this.websocketObj.onopen = function(evt){
                    _this.myWebsocketOnOpen(evt);
                };

                this.websocketObj.onmessage = function(evt){
                    _this.myWebsocketOnMessage(evt);
                };

                this.websocketObj.onerror = function(evt){
                    _this.myWebsocketOnError(evt);
                };


            } catch(e){
                console.log("myWebsocket ERROR : Could not start websocket. Error:");
                console.log(e);
            }
        } else {
            console.log("myWebsocket ERROR : There is already a Websocket running.");
        }
        
        
    }
    
    close(){
        if(this.websocketObj!==undefined){
            try{
                this.websocketObj.close();
                delete this.websocketObj;
            }catch(e){
                console.log("myWebsocket ERROR : Error when tried to close the connection. Error:\n"+e);
            }
            this.count=0;
            this.tempCount=0;
            $(document).trigger("mywebsocket.status.changed",["closed"]);
        }
    }
    
    myWebsocketOnOpen(evt){
        $(document).trigger("mywebsocket.status.changed",["opened"]);
        
    }
    
    myWebsocketOnMessage(evt){
        this.messages.push(JSON.parse(evt["data"]));
        this.tempCount++;
        this.count++;
        $(document).trigger("mywebsocket.message.new",[this.count, this.tempCount, evt]);
        
        
    }
    
    myWebsocketOnError(evt){
        console.log(evt);
        $(document).trigger("message.websocket.error",[this.count, this.tempCount]);
        $(document).on("message.websocket.error",function(){
            
        });
    }
    
    getCount(){
        return this.count;
    }
    getTempCount(){
        return this.tempCount;
    }
    setTempCount(c){
        this.tempCount=c;
    }
    getMessages(){
        return this.messages;
    }
}

$(document).on("mywebsocket.status.changed",function(e,status){
    console.log("myWebsocket EVENT : Status Changed : "+status);
    subscribers=$(".mywebsocket-status-changed-subscriber");
    subscribers.trigger("mywebsocket.status.changed.handler",[status]);
});

$(document).on("mywebsocket.message.new",function(e, c, tc, evt){
    console.log("myWebsocket EVENT : Message New : "+c+" "+" "+tc+" "+evt);
    subscribers=$(".mywebsocket-message-new-subscriber");
    subscribers.trigger("mywebsocket.message.new.handler",[c, tc, evt]);
});
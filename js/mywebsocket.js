/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var output, alerts,old_output,old_toggle;
var mywebsocket;
var mywebsocketObj={
    open: false
};
var mywebsocketCount;
function myWebsocket(id,token){
    
    console.log("myWebsocket()");
    
    $(".accordion-title-number-txt#data-stream").fadeOut();
    //var id=$("#gatewayDetailsModal").attr("gateway-id");
    //var token=$("#gatewayDetailsModal").attr("gateway-token");
    var url="wss://loriot.dacor.de/home/gwtap?gw="+id+"&token="+token;
    output=$(".mywebsocket-output");
    alerts=$(".mywebsocket-alerts");
    old_output=$(".mywebsocket-output-old");
    old_toggle=$(".toggleOldData");
    info=$("mywebsocket-td-info");
    init(url);
    
}

function init(url){
    //notification count auf 0;
    mywebsocketCount=0;
    
    console.log("myWebsocket init(url); url ="+url);
    
    //alte daten werden versteckt. hier einfügen, dass daten, die vlt schon gesammelt wurden in old data geschoben werden
    $(".mywebsocket-old-datas").addClass("mywebsocket-hide");
    
    old_toggle.addClass("mywebsocket-hide");
    mywebsocket= new WebSocket(url);
    
    info.addClass("mywebsocket-hide");
    
    output.empty();
    
    old_output.empty();
    var nodata=$("<tr />",{text:"wait to recieve data..","class":"mywebsocket-nodata","colspan":"14"});
    output.append(nodata);
    
    mywebsocket.onopen = function(evt){
        myWebsocketOnOpen(evt);
    };
    
    mywebsocket.onmessage = function(evt){
        myWebsocketOnMessage(evt);
    };
    
    mywebsocket.onerror = function(evt){
        myWebsocketOnError(evt);
    };
    
    
    
}

function myWebsocketOnOpen(evt){
    console.log("Websocket Connection open");
    setAlert("Websocket Connection open..");
    setAlert("Waiting for message..");
    eventNotify("started"); 
}

function myWebsocketOnMessage(evt){
    //console.log("myWebsocket Got Message");
    setAlert("Websocket got message..");
    var tr=$("<tr />");
    var data=JSON.parse(evt["data"]);
    //console.log(JSON.parse(evt["data"]));
    
    var fields=["cmd","devaddr","mic","fcnt","len","time","sf","rssi","snr","toa","bw","fctrl","port","data"];
    var td;
    var val;
    var add;
    var cls;
    
    console.log(mywebsocketCount);
    if(mywebsocketCount>=20){
        old_output.prepend(output.children()[19]);
        old_toggle.fadeIn();
    } else if(mywebsocketCount<1){
        $(".mywebsocket-nodata").remove();
    } 
    
    fields.forEach(function(f){
        val=data[f];
        add=data[f];
        cls="";
        if(f==="time"){
            val=convertDateInTimeformat(val);
        } else if(f==="data")
        {
            val=val.substring(0,10)+"...";
            cls="hover-datastream-data";
        }
        td=$("<td />",{
            text: val,
            "addition-data": add,
            "class":cls
        });
        tr.append(td);
    });
    
    info.removeClass("mywebsocket-hide");
    console.log(evt);
    output.prepend(tr);
    mywebsocketCount+=1;
    
    $(".accordion-title-number-txt#data-stream").text(mywebsocketCount);
    $(".accordion-title-number-txt#data-stream").fadeIn();
    
    setAlert("Waiting for message..");
    $(document).trigger("message.websocket",[mywebsocketCount]);
}

function myWebsocketOnError(evt){
    console.log("myWebsocket Got Error");
    setAlert("Websocket Errror..");
}

function myWebsocketClose(){
    if(mywebsocket!==undefined){
        console.log("myWebsocket Close");
        mywebsocket.close();
        setAlert("Websocket closed..");
    }
    eventNotify("closed");
}

function eventNotify(status){
    if (status==="started"){
        mywebsocketObj.open=true;
    } else if(status==="closed"){
        mywebsocketObj.open=false;
    } else{
        
    }
    console.log("status in eventNotify");
    console.log(status);
    $(document).trigger("status.websocket.changed",[status]);
}

function setAlert(text){
    //alerts.fadeOut();
    alerts.empty();
    var div=$("<span />",{
        text: text
    });
    alerts.append(div);
    //alerts.fadeIn();
}

$(document).on("mouseover",".mywebsocket-output td",function(){
    $(".mywebsocket-td-info").text($(this).attr("addition-data"));
});

$(document).on("click",".toggleOldData",function(){
    
    if ($(".mywebsocket-old-datas").hasClass("mywebsocket-hide")){
        $(".mywebsocket-old-datas").fadeIn();
        $(".mywebsocket-old-datas").removeClass("mywebsocket-hide");
    } else {
        $(".mywebsocket-old-datas").fadeOut();
        $(".mywebsocket-old-datas").addClass("mywebsocket-hide");
    }
});


/*$(document).on("status.websocket.changed",function(e, status){
    
    subscribers=$(".mywebsocket-status-subscriber");
    
    console.log("subscribers");
    console.log(subscribers);
    console.log("status event trigger");
    console.log(status);
    subscribers.trigger("status.websocket.handler",[status]);
});*/

/*$(document).on("message.websocket",function(e, status){
    
    subscribers=$(".mywebsocket-message-subscriber");
    
    console.log("subscribers");
    console.log(subscribers);
    console.log("message event trigger");
    console.log(status);
    subscribers.trigger("message.websocket.handler",[status]);
});*/
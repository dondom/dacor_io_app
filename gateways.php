<?php

        include "php/config.php";
        
	#Rest API Call um alle Gateways zu bekommen

	//API CALL für alle Gateways
	$curl = curl_init("https://loriot.dacor.de/1/nwk/gateways?perPage=100");
	curl_setopt($curl, CURLOPT_URL, "https://loriot.dacor.de/1/nwk/gateways?perPage=100");
        
	$headers = [
		"Authorization: Bearer ".$apiKey
	];
        
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$gateways=curl_exec($curl);
	curl_close($curl);

	$gateways=json_decode($gateways);
	#var_dump($return);

	$js_str="";


?>


<html>

	<head>      
		<!-- Stylesheets -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
                <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css"/>
                <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
                <!--<link rel="stylesheet" href="css/datatable.css">-->
		<link rel="stylesheet" href="css/style.css">
                <link rel="stylesheet" href="css/mymodal.css">
                <link rel="stylesheet" href="css/mypagination.css">
                
                <!--Javascripts -->
                    <!-- jQuery, jQuery UI & Datatable -->
                <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
                 <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
                    <!-- Bootstrap & Popper -->
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>           
               
                <!-- Font Awesome -->
                <script src="https://kit.fontawesome.com/2fa0037958.js" crossorigin="anonymous"></script>
               
                <link rel="stylesheet" href="css/ol.css" type="text/css">
                <script src="js/ol.js"></script>
                <script src="js/mytime.js"></script>
                <!--<script src="js/mywebsocket.js"></script>-->
                <script src="js/myWebsocket_new.js"></script>
                <script src="js/mypagination.js"></script>
                <script src="js/mymodal.js"></script>
                <script src="js/mymodal-events.js"></script>
                <script src="js/functions.js"></script>

		<title>Dacor.io - App</title>
	</head>
	
	<body>
		
                <div class="content-container">
                <h1>Gateways</h1>
		<table id="tableGateways" class="table">
			<thead id="tablehead" class="table-dark">
				<tr>
                                    <td>Model</td>
					<td>Name</td>
					<td>MAC</td>
					<td>Model</td>
					<td>Version</td>
					<td>Status</td>
					<td>Last Data</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				$js_str="{";
					foreach($gateways->{"gateways"} as $gw){

						//API CALL für Gateways details
						$curl = curl_init("https://loriot.dacor.de/1/nwk/gateway/".$gw->{"_id"});

						curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						$gatewayDetails=curl_exec($curl);
						curl_close($curl);

						$js_str.='"'.$gw->{"_id"}.'":'.$gatewayDetails.',';
						
						echo "<tr>\n";
                                                echo '<td class="align-middle"><img class="gatewaysImg" src="img/modele/'.$gw->{"base"}.'-'.$gw->{"model"}.'.png"></img></td>';
						echo '<td class="gatewayDetails align-middle" data-target="#gatewayDetailsModal" data="'.$gw->{"_id"}.'"><span class="fakeLink">'.$gw->{"title"}.'</span></td>'."\n";
						echo "<td class='align-middle'>".$gw->{"MAC"}."</td>"."\n";
						echo "<td class='align-middle'>".$gw->{"model"}."</td>"."\n";
						echo "<td class='align-middle'>".$gw->{"version"}."</td>"."\n";
						if($gw->{"connected"}=="1"){
							echo '<td class=" align-middle status green">Online</td>'."\n";
						} elseif($gw->{"connected"}=="0"){
							echo '<td class=" align-middle status red">Offline</td>'."\n";
						}else {
							echo '<td class="align-middle">nicht verfügbar</td>'."\n";
						}
						echo "<td class='table-lastData align-middle'>".$gw->{"lastData"}."</td>"."\n";
						echo "</tr>"."\n";
					}

					$js_str[strlen($js_str)-1]="}";

					echo '<script> var allGatewayDetails=\''.$js_str.'\';</script>'."\n";
					
				?>
			</tbody>
		</table>
            </div>
		<!-- Modal Window -->

		<div class="modal fade mymodal" id="gatewayDetailsModal" tabindex="-1" role="dialog" aria-labelledby="gatewayDetailsModal" aria-hidden="true">
  			<div class="modal-dialog modal-xl" role="document">
    			<div class="modal-content">
                            <div class="modal-header5">
      					
        				
                            </div>
                            <div class="mymodal-header">
                                
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                
                                <div class="row "> <!-- align-items-center -->
                                    <div class="col-sm-4">
                                        <img class="img-thumbnail rounded float-left mymodal-image" src=""></img>
                                        <div class="basemodel"><span class="basename"></span> <span class="modelname"></span>
                                            <div>Version: <span class="version"></span></div></div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="mymodal-title" id="exampleModalLongTitle">
                                            <span class="mymodal-title-txt"></span>
                                            <span class="interval-options">
                                                
                                            </span>
                                        </div>
                                        <div class="mymodal-connected online">
                                            <span class="mymodal-connected-circle pulse online"></span>
                                            <span class="mymodal-connect-text"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">Configuration</div>
                                        </div>
                                        <div class="row details-configurations">
                                            <div class="col-sm-4 configuration community">Community access<span class="value"></span></div>
                                            <div class="col-sm-4 configuration alerts">Alert Notifications<span class="value"></span></div>
                                            <div class="col-sm-4 configuration ignore">Ignore Data<span class="value"></span></div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:5px;">
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Latency: </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <div>
                                                    <span class="latency-value"></span>ms
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="row" style="margin-top:5px;">
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Last Data: </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <div>
                                                    <span class="last-data-value"></span><span></span>
                                                </div>
                                                <div>
                                                    <span class="last-data-value-date" style="font-size: 8pt; font-style: italic;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:5px;">
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Last connect: </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <div>
                                                    <span class="last-connect-value"></span><span></span>
                                                </div>
                                                <div>
                                                    <span class="last-connect-value-date" style="font-size: 8pt; font-style: italic;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row" style="margin-top:5px;">
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Last Keep Alive: </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <div>
                                                    <span class="last-pong-value"></span> <span></span>
                                                </div>
                                                <div>
                                                    <span class="last-pong-value-date" style="font-size: 8pt; font-style: italic;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="row" style="margin-top: 15px; display:none;">
                                            <div class="col-sm-4">
                                                <div><span class="info-point-title">Latency: </span></div>
                                                <div><span class="info-point-title">Last Data: </span></div>
                                                <div><span class="info-point-title">Last connect: </span></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div><span class="latency-value"></span>ms</div>
                                                <div><span class="last-data-value"></span><span> secs ago..</span>
                                                <div class="last-data-value-date">1.1.1970</div>
                                                </div>
                                                
                                                <div></div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="row header-informations" style="display:none;">
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Latency: </span><span class="latency-value"></span>ms
                                            </div>
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Last Data: </span><span class="last-data-value"></span><span> secs ago..</span>
                                            </div>
                                            <div class="col-sm-4">
                                                <span class="info-point-title">Last connect: </span><span class="last-connect-value"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                 
                                
                                        
                            </div>
                            <div class="modal-body mymodal-body">
                                
                                <div id="details-accordion">
                                    <h3>
                                        <span class="accordion-title">Details</span>
                                    </h3>
                                    <div class="status-infos">
                                        
                                        
                                        <!-- Neue Infos 
                                        <!-- Adresse 
                                        Ip
                                        Mac
                                        Uptime Downtime Chart
                                        
                                        -->
                                        
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h5 style="font-weight: bold;">Connection details</h5>
                                                <div class="info-point">
                                                    <div class="info-point-title">EUI: </div>
                                                    <div><span class="eui-value"></span> <span class="copy">&#128203;</span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">MAC: </div>
                                                    <div class="mac-value"></div>
                                                </div>
                                                <div class="info-point">
                                                    <div class="info-point-title">Connected from IP: </div>
                                                    <div class="ip-value"></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Roaming </div>
                                                    <div class="roaming-value"></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Networks </div>
                                                    <div class="networks-value">
                                                        
                                                    </div>
                                                </div><!-- comment -->

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <h5 style="font-weight: bold;">Location</h5>
                                                <div>
                                                    <span class="address-value"></span>
                                                </div><!-- comment -->
                                                <div>
                                                    <span class="zip-value"></span> <span class="city-value"> </span> <span class="country-value"></span>
                                                </div><!-- comment -->
                                                <div>
                                                    
                                                </div>
                                                <div style="display:none;">
                                                    <div class="lat-value"></div>
                                                    <div class="lon-value"></div>
                                                </div>
                                                <div class="map-container">
                                                    <div class="map" id="map"></div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <h5 style="font-weight: bold;">Statistics</h5>
                                                
                                                <div class="uptimeChart" style="display: none;">
                                                    <div class="item html">
                                                        <h2>Uptime</h2>
                                                        <svg width="160" height="160" xmlns="http://www.w3.org/2000/svg">
                                                        <g>
                                                            <title>Layer 1</title>
                                                            <circle id="circle" class="circle_animation" r="60" cy="81" cx="81" stroke-width="25" stroke="#3c8dbc" fill="none"/>
                                                        </g>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>
                                        <span class="accordion-title">mehr Details</span>
                                    </h3>
                                    <div class="status-infos">
                                        <div class="row">                                     
                                            <div class="col-sm-4">
                                                <h5 style="font-weight:bold;">Hardware</h5>
                                                <div class="info-point">
                                                    <div class="info-point-title">Concentratorname</div>
                                                    <div><span class="concentrator-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Bus bzw connected over</div>
                                                    <div><span class="bus-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Card</div>
                                                    <div class="card-value"></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">SSH Tunnel Service Server</div>
                                                    <div><span class="sshtunnelserviceserver-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Remote time offset</div>
                                                    <div>~ <span class="remotetimeoffset-value"></span> sec</div>
                                                </div><!-- comment -->
                                            </div>
                                            <div class="col-sm-4">
                                                <h5 style="font-weight: bold;">Antennas</h5><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Region</div>
                                                    <div><span class="region-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Radiobands</div>
                                                    <div class="radioband-value"></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">MaxAntennas</div>
                                                    <div><span class="maxantennas-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">MaxChannels</div>
                                                    <div><span class="maxchannels-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">MaxChannelPlans</div>
                                                    <div><span class="maxchannelplans-value"></span></div>
                                                </div><!-- comment -->
                                            </div>
                                            <div class="col-sm-4">
                                                <h5 style="font-weight: bold;">Maschine Infos</h5><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Machine</div>
                                                    <div><span class="machine-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Name</div>
                                                    <div><span class="machine-name-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Release</div>
                                                    <div><span class="machine-release-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">System</div>
                                                    <div><span class="machine-sys-value"></span></div>
                                                </div><!-- comment -->
                                                <div class="info-point">
                                                    <div class="info-point-title">Version</div>
                                                    <div><span class="machine-version-value"></span></div>
                                                </div><!-- comment -->
                                            </div>
                                        </div>  
                                        
                                        
                                        
                                        
                                    </div>
                                    <h3 class="accordion-alerts">
                                        <span class="accordion-title">Alerts</span>
                                    </h3>
                                    
                                    <div class="alerts-container">
                                        <div class="mypagination-alerts">
                                            
                                        </div> 
                                    </div>
                                    <h3>
                                        <span class="accordion-title">Logs</span>
                                    </h3>
                                    <div class="logs-container">
                                        <div class="mypagination-logs">
                                            
                                        </div>
                                    </div>
                                    <h3>
                                        <span class="accordion-title">Data Stream</span>
                                        <span>
                                            <span class="accordion-title-number mywebsocket-status-changed-subscriber" id="data-stream">
                                                <span class="accordion-title-number-button-container">
                                                    <span class="accordion-title-number-button icon-play" action="websocket-start">
                                                        <img src="img/icons/play_icon.svg" class="icon-play"/>
                                                    </span>
                                                    <span class="accordion-title-number-button icon-stop" action="websocket-stop">
                                                        <img src="img/icons/stop_icon.svg" class="icon-stop"/>
                                                    </span>
                                                    <span class="accordion-title-number-button icon-refresh rotate-center" action="none">
                                                        <img src="img/icons/refresh_icon.svg" class="icon-refresh"/>
                                                    </span>
                                                </span>
                                                <!--<img src="img/icons/refresh_icon.svg" class="accordion-title-number-refresh rotate-center"></img>-->
                                                <span class="accordion-title-number-txt mywebsocket-message-new-subscriber"></span>
                                            </span>
                                        </span>
                                    </h3>
                                    <div class="datastream-container">
                                        <!--<div><span>Anzahl Nachrichten:</span> <span class="mypagination-anzahl-nachrichten mywebsocket-message-subscriber"></span></div>-->
                                        <div class="mypagination-datastream">
                                            
                                        </div>
                                    </div>
                                    <h3>
                                        <span class="accordion-title">Devices</span>
                                    </h3>
                                    <div class="devices-container">
                                        <div class="mypagination-devices">
                                            
                                        </div>
                                    </div>
                                    <h3>
                                        <span class="accordion-title">Aktionen</span>
                                    </h3>
                                    <div>   
                                    </div>
                                </div>
                                
                                
                                
                                <div class="container">
                                    <div class="d-flex justify-content-center">
                                        <a href="#moreDetailsCollapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">weitere Details...</a>
                                    </div>
                                </div>
        			
        			<div class="collapse" id="moreDetailsCollapse">
                                    <div class="card card-body">
                                        
                                    </div>
      				</div>
      				<div class="modal-footer5">
        				<!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        				<button type="button" class="btn btn-primary">Save changes</button>-->
      				</div>
    			</div>
  			</div>
		</div>


	</body>
</html>